/**
 * Created by dev01 on 10.02.16.
 */
var search = document.getElementsByClassName('selector-find')[0];
var nextBtn = document.getElementsByClassName('selector-next')[0];
var prevBtn = document.getElementsByClassName('selector-prev')[0];

var parentBtn = document.getElementsByClassName('nav-top')[0];
var childBtn = document.getElementsByClassName('nav-bottom')[0];
var leftBtn = document.getElementsByClassName('nav-left')[0];
var rightBtn = document.getElementsByClassName('nav-right')[0];

var obj = [];

function getElements(matchClass) {
    var res = [];
    var elems = document.getElementsByTagName('*'), i;
    for (i in elems) {
        if (elems[i].className) {
            if( ((elems[i].className).indexOf(matchClass) > -1) ||
                ((elems[i].id).indexOf(matchClass) > -1)) {
                res.push(elems[i]);
            }
        }
    }
    return res;
}

function cycleElement(direction) {
    var done = false;
    if ((direction !== 'prev') && (direction !== 'next')) {
        return false;
    }

    [].forEach.call(obj, function( el, index, arr ) {
        if ((el.classList.contains('founded')) && (done === false)) {
            obj[index].classList.remove('founded');
            switch(index) {
                case 0:
                    if (direction === 'prev') {
                        obj[obj.length - 1].classList.add('founded');
                    } else if (direction === 'next') {
                        obj[index + 1].classList.add('founded');
                    }
                    break;
                case obj.length - 1:
                    if (direction === 'prev') {
                        obj[index - 1].classList.add('founded');
                    } else if (direction === 'next') {
                        obj[0].classList.add('founded');
                    }
                    break;
                default:
                    if (direction === 'prev') {
                        obj[index - 1].classList.add('founded');
                    } else if (direction === 'next') {
                        obj[index + 1].classList.add('founded');
                    }
                    break;
            }
            done = true;
        }
    });
}

function gotoElement(direction) {
    var i, el;
    var elems = document.getElementsByTagName('*');
    for (i in elems) {
        if ((elems[i].classList.contains('founded')) ) {
            switch(direction) {
                case 'parent':
                    el = elems[i].parentNode;
                    break;
                case 'child':
                    el = elems[i].children[0];
                    break;
                case 'prev':
                    el = elems[i]. previousElementSibling;
                    break;
                case 'next':
                    el = elems[i].nextElementSibling;
                    break;
            }
            if (el.tagName !== undefined) {
                elems[i].classList.remove('founded');
                el.classList.add('founded');
                break;
            } else {
                break;
            }
        }
    }
}

function disablePrevNext() {
    document.getElementsByClassName('selector-next')[0].disabled='disabled';
    document.getElementsByClassName('selector-prev')[0].disabled='disabled';
}

function disableAll() {
    document.getElementsByClassName('selector-next')[0].disabled='disabled';
    document.getElementsByClassName('selector-prev')[0].disabled='disabled';

    document.getElementsByClassName('nav-top')[0].disabled='disabled';
    document.getElementsByClassName('nav-bottom')[0].disabled='disabled';
    document.getElementsByClassName('nav-left')[0].disabled='disabled';
    document.getElementsByClassName('nav-right')[0].disabled='disabled';
}

nextBtn.addEventListener('click', function() {
    cycleElement('next');
});
prevBtn.addEventListener('click', function() {
    cycleElement('prev');
});

parentBtn.addEventListener('click', function() {
    disablePrevNext();
    gotoElement('parent');
});
childBtn.addEventListener('click', function() {
    disablePrevNext();
    gotoElement('child');
});
leftBtn.addEventListener('click', function() {
    disablePrevNext();
    gotoElement('prev');
});
rightBtn.addEventListener('click', function() {
    disablePrevNext();
    gotoElement('next');
});


search.addEventListener('click', function() {
    var tempObjects = document.getElementsByClassName('founded');
    [].forEach.call(tempObjects, function( el ) {
        el.classList.remove('founded');
    });
    disableAll();

    var target = document.getElementsByClassName('selector')[0];
    if (document.getElementsByClassName(target.value)) {
        obj = getElements(target.value);
        if (obj.length > 1) {
            document.getElementsByClassName('selector-next')[0].disabled='';
            document.getElementsByClassName('selector-prev')[0].disabled='';

            document.getElementsByClassName('nav-top')[0].disabled='';
            document.getElementsByClassName('nav-bottom')[0].disabled='';
            document.getElementsByClassName('nav-left')[0].disabled='';
            document.getElementsByClassName('nav-right')[0].disabled='';
        }
        obj[0].classList.add('founded');
    }
}, false);